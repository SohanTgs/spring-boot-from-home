package com.example.spring.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class MainController {

	@RequestMapping({"/","/index"})
	public String index() {
		return "index.html";
	}
	
	@RequestMapping("/user_register")
	public String userLogin() {
		return "user_register.html";
	}
}
